<?php 

require_once 'includes/operacoes_banco.php';
require_once 'includes/operacoes_sessao.php';
require_once 'includes/functions.php';
require 'header.php';

if($_POST){

	$nome = $_POST['nome'];
	$usuario = $_POST['usuario'];
	$email = $_POST['email'];
	$senha = md5($_POST['senha']);

	$query = "SELECT * FROM usuarios WHERE email = '$email' OR usuario = '$usuario'";

	$result = getSingleResult($query);
	if(count($result)){
		setFlashMessage('erro', 'Usuario ou email ja cadastrado(s)!');
		redirect('criar_usuario.php');
		die();
	} else {

	$query = <<<SQL
INSERT INTO usuarios
	(nome, email, senha, usuario)
VALUES
	("$nome", "$email", "$senha", "$usuario");
SQL;

	executaQuery($query);
}
}

 ?>
<div class="container">
	<div class="col-12 col-sm-6 offset-sm-3 mt-3">
		<div class="card">
		<h1 class="card-header display-6" style="text-align: center;">Cadastrar</h1>
		<div class="card-block">
		<form method="post">
			<div class="form-group">
				<label>Nome: </label>
				<input type="text" name="nome" class="form-control" />
			</div>
			<div class="form-group">
				<label>Usuario: </label>
				<input type="text" name="usuario" class="form-control" />
			</div>
			<div class="form-group">
				<label>Email: </label>
				<input type="text" name="email" class="form-control">
			</div>
			<div class="form-group">
				<label>Senha: </label>
				<input type="password" name="senha" class="form-control">
			</div>
			<div class="form-group"><button type="submit" class="btn btn-primary form-control">Cadastrar</button></div>
		</form>
		<p style="text-align: center;"><a href="index.php" >Ja possui cadastro? Faca o login!</a></p>
		</div>
		</div>
	</div>
</div>
<?php require 'footer.php'; ?>