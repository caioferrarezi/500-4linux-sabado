<?php 

session_start();

function verificarLogin(){
	if(!isset($_SESSION['usuario'])){
		setFlashMessage('erro', 'Voce nao tem permissao, faca o login.');
		header('Location: index.php');
		die();
	}
}

function logout(){
	session_destroy();
	header('Location: index.php');
}

function login(array $usuario, $pagina = 'inicio.php'){
	$_SESSION['usuario'] = $usuario;
	header('Location: ' . $pagina);
}

function setFlashMessage($id, $message){
	if(! isset($_SESSION['messages'])){
		$_SESSION['messages'] = [];
	}
	$_SESSION['messages'][$id] = $message;
}

function getFlashMessage($id){
	$msg = null;
	if(isset($_SESSION['messages'][$id])){
		$msg = $_SESSION['messages'][$id];
		unset($_SESSION['messages'][$id]);
	}
	return $msg;
}

