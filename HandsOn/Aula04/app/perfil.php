<?php 
	
require_once 'includes/operacoes_banco.php';
require 'includes/operacoes_sessao.php';
require 'includes/functions.php';
require 'header.php';

if($_GET) :
	$user = $_GET['usuario'];
	$query = "SELECT * FROM usuarios WHERE usuario = '$user'";
	$user_perfil =  getSingleresult($query);
	if(!empty($user_perfil)) :
	$query = <<<SQL
	SELECT * FROM seguindo 
	WHERE 
		meu_id = {$_SESSION['usuario']['id']}
		AND seu_id = {$user_perfil['id']}
SQL;
	if($_SESSION['usuario']['usuario'] != $user_perfil['usuario']){
		if(!!count(getResults($query))){
			$seguir = " <a class='btn btn-sm btn-primary' href='operacao.php?op=unfollow&u_id=" . $user_perfil['id'] . "&user=".$user_perfil['usuario']."'>Deixar de Seguir</a>";
		} else{
			$seguir = " <a class='btn btn-sm btn-primary' href='operacao.php?op=follow&u_id=" . $user_perfil['id'] . "&user=".$user_perfil['usuario']."'>Seguir</a>";
		}
	} else {
		$seguir = '';
	}
?>
	<div class="container">
	<div class="jumbotron mt-3">
	<div class="col-md-4" style="margin: auto;">
		<div class="card d-block text-center">
			<img class="card-img-top" src="https://placeholdit.imgix.net/~text?txtsize=33&txt=300x300&w=500&h=500" style="width: 100%;">
			<div class="card-header">
				<h1 style="font-size: 1.3em;">@<?= $user_perfil['usuario']?></h1>
				<?= $seguir; ?>
			</div>
		</div>
	</div>
	</div>

<?php 
	$query = "SELECT * FROM mensagens AS m INNER JOIN usuarios AS u ON u.id = m.usuario_id WHERE u.usuario = '$user' ORDER BY m.id DESC";
	$mensagens = getResults($query);
		if(!empty($mensagens)) :
			foreach($mensagens as $mensagem) :
		?>
	<div class="col-md-6 offset-md-3">
		<div class="card mb-3">
			<p class="card-header"><strong><?= $mensagem['nome']; ?></strong></p>
			<div class="card-block">
				<p class="card-text"><?php echo preg_replace('/\@([a-zA-Z0-9_\-\.]+)/', '<a href="perfil.php?usuario=$1">@$1</a>', $mensagem['mensagem']); ?></p>
			</div>
		</div>
	</div>
	<?php 
			endforeach;
		else :
			echo "<p>Nao existem mensagens no momento ):</p>";
		endif;
	else:
	setFlashMessage('erro', 'Usuario nao Encontrado!');
	redirect('inicio.php');
	endif;
endif;
?>
	</div>
<?php
require 'footer.php';
?>

