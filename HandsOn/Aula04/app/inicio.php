<?php

require_once 'includes/operacoes_banco.php';
require 'includes/operacoes_sessao.php';
require 'includes/functions.php';
require 'header.php';

verificarLogin();

$user_id = $_SESSION['usuario']['id'];

if(isset($_POST['mensagem'])){	
	$mensagem = addslashes($_POST['mensagem']);

	/*$query = "INSERT INTO mensagens (usuario_id, mensagem) VALUES (" . $_SESSION['usuario']['id'] . ", " . $mensagem . ");";*/

	$query = <<<SQL
		INSERT INTO mensagens
			(usuario_id, mensagem)
		VALUES
			($user_id, '$mensagem');
SQL;
	
	executaQuery($query);
	redirect('inicio.php');
	die();
}

?>
	<div class="container">
		<div class="jumbotron mt-3">
			<h2 class="display-4">Ola, <?php echo $_SESSION['usuario']['nome']; ?>!</h2>
			<hr class="my-4">
			<p class="lead">No que voce esta pensando?</p>

			<form method="post">
				<div class="form-group"><textarea rows="4" name="mensagem" maxlength="140" required class="form-control"></textarea></div>
				<button type="submit" class="btn btn-primary">Enviar Mensagem</button>
			</form>
		</div>
	</div>
	<div class="container">
	<?php 
	$query = "SELECT * FROM mensagens AS m INNER JOIN usuarios AS u ON u.id = m.usuario_id WHERE m.usuario_id = ". $_SESSION['usuario']['id'] ." OR m.usuario_id IN (SELECT seu_id FROM seguindo WHERE meu_id = ". $_SESSION['usuario']['id'] .") ORDER BY m.id DESC";
	$mensagens = getResults($query);
	if(!empty($mensagens)) :
		foreach($mensagens as $mensagem) :
	?>
		<div class="card mb-3">
			<p class="card-header"><strong><?= $mensagem['nome']; ?></strong></p>
			<div class="card-block">
				<p class="card-text"><?php echo preg_replace('/\@([a-zA-Z0-9_\-\.]+)/', '<a href="perfil.php?usuario=$1">@$1</a>', $mensagem['mensagem']); ?></p>
			</div>
		</div>
	<?php 
		endforeach;
	else :
		echo "<p>Nao existem mensagens no momento ):</p>";
	endif;
	?>
	</div>
<?php require 'footer.php'; ?>
