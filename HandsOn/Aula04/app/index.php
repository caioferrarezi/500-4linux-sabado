<?php 

require 'includes/operacoes_sessao.php';
require 'includes/operacoes_banco.php';
require 'includes/functions.php';
require 'header.php';

if(isset($_GET['logout'])){
	logout();
}

if($_POST){
	$usuario = $_POST['usuario'];
	$senha = md5($_POST['senha']);

	$query = <<<SQL
SELECT * FROM usuarios 
WHERE usuario = "$usuario" 
AND senha = "$senha"
LIMIT 1
SQL;
	$usuario = getSingleResult($query);
	if(!count($usuario)){
		setFlashMessage('erro', 'Login sem sucesso');
		redirect('index.php');
		die();
	}	
	login($usuario);
}

?>
	<div class="container">
		<div class="col-12 col-sm-6 offset-sm-3 mt-3">
			<div class="card">
			<h1 class="card-header display-6" style="text-align: center;">Login</h1>
			<div class="card-block">
			<form method="post">
				<div class="form-group">
					<label class="col-form-label">Usuario: </label>
					<input type="text" name="usuario" class="form-control">
				</div>
				<div class="form-group">
					<label>Senha: </label>
					<input type="password" name="senha" class="form-control">
				</div>
				<div class="form-group"><button type="submit" class="btn btn-primary form-control">Entrar</button></div>
			</form>
			<p style="text-align: center;"><a href="criar_usuario.php">Registre-se Aqui</a></p>
			</div>
			</div>
		</div>
	</div>

<?php require 'footer.php'; ?>