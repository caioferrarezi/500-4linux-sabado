<?php

define('PI', 3.1416);

$pi = 3.1416;

function somar($valor){
	global $pi;

	$pi = 4.50; //nao altera o valor da variavel definida fora da funcao

	return $valor + $pi;

	//return $valor + PI;
}

var_dump($pi);

var_dump(somar(54));

echo '<hr>';

function inverter(&$a, &$b){ // referencias as variaveis com o &
	$aux = $a;
	$a = $b;
	$b = $aux;
}

// dessa maneira a variavei e' passada para a funcao permitindo a troca do seu valor

$a = 10;
$b = 20;

var_dump($a, $b);

inverter($a, $b);

var_dump($a, $b);

?>