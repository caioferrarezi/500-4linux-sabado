<?php 

	// casting = mudanca de tipagem da variavel

	$var = 10;
	var_dump($var);

	$var = (boolean) $var; //numero diferente 0 sempre sera true
	var_dump($var);

	$var = (int) $var;
	var_dump($var);

	$var = (float) $var;
	var_dump($var);

	$var = (string) $var;
	var_dump($var);

	$var = (array) $var;
	var_dump($var);
?>