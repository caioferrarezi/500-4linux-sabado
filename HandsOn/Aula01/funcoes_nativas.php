<?php 

$var = '20';

echo 'Variavel $var e array? ' . (is_array($var) ? 'Sim' : 'Nao') . '<br>';
echo 'Variavel $var e bool? ' . (is_bool($var) ? 'Sim' : 'Nao') . '<br>';
echo 'Variavel $var e string? ' . (is_string($var) ? 'Sim' : 'Nao') . '<br>';
echo 'Variavel $var e float? ' . (is_float($var) ? 'Sim' : 'Nao') . '<br>';
echo 'Variavel $var e object? ' . (is_object($var) ? 'Sim' : 'Nao') . '<br>';
echo 'Variavel $var e integer? ' . (is_integer($var) ? 'Sim' : 'Nao') . '<br>';

?>