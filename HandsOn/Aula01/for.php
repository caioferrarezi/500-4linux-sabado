<?php 

for($contador = 1; $contador <= 4 ; $contador++){
	$quadrado = $contador * $contador;
	echo $contador . " ao quadrado = " . $quadrado . '<br>';
}

echo '<br>';

for($c = 1; $c <= 10 ; $c += 2){
	echo $c . '<br>';
}

echo '<br>';

for($c = 1; $c <= 10 ; $c++){
	if($c % 2 == 0) { continue; }
	echo $c . '<br>';
}

?>