<?php 

function soma($a, $b = 10){ // variavel b com valor default
	return $a + $b;
}

$resultado = soma(1, 2);

var_dump($resultado);
var_dump(soma($resultado)); //soma ao valor default da variavel b
var_dump(soma('bicho papao')); //ignora a string e retorna o 10 default da variavel b
var_dump(soma('32 bicho papao')); //soma o numero presente na string

?>