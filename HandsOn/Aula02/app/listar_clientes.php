<?php 
	require_once 'verifica_login.php';

	$clientes = [
	[
		'id' => 1,
		'nome' => 'Jose',
	],
	[
		'id' => 2,
		'nome' => 'Edigardo',
	],
	];
?>

<html>
<head>
	<title>Listar Clientes</title>
</head>
<body>
	<h1>Listar Clientes</h1>
	<table>
		<tr>
			<th>ID</th>
			<th>Nome</th>
		</tr>
		<?php foreach($clientes as $cliente) : ?>
		<tr>
			<td><?php echo $cliente['id']; ?></td>
			<td><?php echo $cliente['nome']; ?></td>
		</tr>
		<?php endforeach; ?>
	</table>

	<?php require_once 'menu.php'; ?>
</body>
</html>