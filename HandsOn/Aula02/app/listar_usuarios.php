<?php 
	require_once 'verifica_login.php';

	$usuarios = [
	[
		'id' => 1,
		'nome' => 'Jose',
	],
	[
		'id' => 2,
		'nome' => 'Edigardo',
	],
	];
?>

<html>
<head>
	<title>Listar Usuarios</title>
</head>
<body>
	<h1>Listar Usuarios</h1>
	<table>
		<tr>
			<th>ID</th>
			<th>Nome</th>
		</tr>
		<?php foreach($usuarios as $usuario) : ?>
		<tr>
			<td><?php echo $usuario['id']; ?></td>
			<td><?php echo $usuario['nome']; ?></td>
		</tr>
		<?php endforeach; ?>
	</table>

	<?php require_once 'menu.php'; ?>
</body>
</html>