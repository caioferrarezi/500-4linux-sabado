<?php 
	require_once 'verifica_login.php';

	$vendedores = [
	[
		'id' => 1,
		'nome' => 'Jose',
	],
	[
		'id' => 2,
		'nome' => 'Edigardo',
	],
	];
?>

<html>
<head>
	<title>Listar Vendedores</title>
</head>
<body>
	<h1>Listar Vendedores</h1>
	<table>
		<tr>
			<th>ID</th>
			<th>Nome</th>
		</tr>
		<?php foreach($vendedores as $vendedor) : ?>
		<tr>
			<td><?php echo $vendedor['id']; ?></td>
			<td><?php echo $vendedor['nome']; ?></td>
		</tr>
		<?php endforeach; ?>
	</table>

	<?php require_once 'menu.php'; ?>
</body>
</html>