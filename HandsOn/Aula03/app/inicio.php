<?php

require_once 'includes/operacoes_banco.php';
require 'includes/operacoes_sessao.php';
require 'includes/functions.php';
require 'header.php';

verificarLogin();

$user_id = $_SESSION['usuario']['id'];

if(isset($_POST['mensagem'])){	
	$mensagem = addslashes($_POST['mensagem']);

	/*$query = "INSERT INTO mensagens (usuario_id, mensagem) VALUES (" . $_SESSION['usuario']['id'] . ", " . $mensagem . ");";*/

	$query = <<<SQL
		INSERT INTO mensagens
			(usuario_id, mensagem)
		VALUES
			($user_id, '$mensagem');
SQL;
	
	executaQuery($query);
	redirect('inicio.php');
	die();
}

?>
	<h2>Ola, <?php echo $_SESSION['usuario']['nome']; ?></h2>

	<p></p>

	<form method="post">
		<textarea rows="4" cols="35" name="mensagem" maxlength="140" required></textarea><br><br>
	<button type="submit">Enviar Mensagem</button>

	<?php 
	$query = "SELECT * FROM mensagens AS m INNER JOIN usuarios AS u ON u.id = m.usuario_id ORDER BY m.id DESC";
	$mensagens = getResults($query);
	if(!empty($mensagens)) :
		foreach($mensagens as $mensagem) :
	?>
	<div style="border: 1px solid #444; margin-top: 10px; padding: 10px;">
		<p style="padding: 0; margin: 0;"><strong><?= $mensagem['nome']; ?></strong>: <?php echo preg_replace('/\@([a-zA-Z0-9_\-\.]+)/', '<a href="perfil.php?usuario=$1">@$1</a>', $mensagem['mensagem']); ?></p>
	</div>
	<?php 
		endforeach;
	else :
		echo "<p>Nao existem mensagens no momento ):</p>";
	endif;
	?>
</form>
<?php require 'footer.php'; ?>
